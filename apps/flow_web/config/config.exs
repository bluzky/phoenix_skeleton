# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :flow_web,
  namespace: FlowWeb,
  ecto_repos: [Flow.Repo]

# Configures the endpoint
config :flow_web, FlowWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hZ5ZQAmFx3kT0IcI0m8EMFFSMpv3xNMGksgQ4L0ZT6cgIaO7j1S1hOxMComj5yxI",
  render_errors: [view: FlowWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: FlowWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :flow_web, :generators, context_app: :flow

config :flow_web, Flow.Guardian,
  issuer: "flow_web",
  secret_key: System.get_env("ADMIN_SECRET_KEY")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
