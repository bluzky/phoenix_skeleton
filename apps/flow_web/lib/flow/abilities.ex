defimpl Can, for: Flow.Account.User do
  alias Flow.Account.User
  alias Flow.Organization.{Company}

  @doc """
  Check ability for action on a con troller
  """
  def can?(%User{roles: roles}, action, controller)
      when is_atom(action) and is_atom(controller) do
    Enum.reduce(roles, false, fn role, authorized ->
      authorized or _can?(role, action, controller)
    end)
  end

  @doc """
  Check ability for action on object
  """
  # def can?(%User{roles: roles, id: id}, _action, %Company{supported_by_id: supporter_id}) do
  #   "manager" in roles or id == supporter_id
  # end

  defp _can?("admin", _action, _object), do: true

  defp _can?("hr", action, FlowWeb.UserController) do
    action not in [:delete]
  end

  defp _can?(_role, _action, _object), do: false
end
