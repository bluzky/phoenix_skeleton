defmodule Flow.Guardian do
  use Guardian, otp_app: :flow_web

  def subject_for_token(resource, _claims) do
    sub = to_string(resource.id)
    {:ok, sub}
  end

  def subject_for_token(_, _) do
    {:error, :invalid_resource}
  end

  def resource_from_claims(claims) do
    # Here we'll look up our resource from the claims, the subject can be
    # found in the `"sub"` key. In `above subject_for_token/2` we returned
    # the resource id so here we'll rely on that to look it up.
    id = claims["sub"]

    resource = Flow.Account.get_user(id) |> Flow.Repo.preload([:company, :department])

    if resource do
      {:ok, resource}
    else
      {:error, :invalid_claims}
    end
  end
end
