defmodule Gaco.ResourceLoader do
  alias Flow.Repo
  alias Flow.Organization.{Company, Department}
  alias Flow.Account.User

  def load(Company = model, id) do
    Repo.get(model, id)
  end

  def load(Department = model, id) do
    Repo.get(model, id)
    |> Repo.preload(:company)
  end

  def load(User = model, id) do
    Repo.get(model, id)
    |> Repo.preload([:company, :department])
  end

  def load(model, id) do
    Repo.get(model, id)
  end
end
