defmodule Gaco.ResourceNotFound do
  @moduledoc """
  Exception raised when cannot load resource
  """
  defexception plug_status: 404, message: "No resouce found"
end

defmodule Gaco.AccessDenied do
  @moduledoc """
  Exception raised when cannot load resource
  """
  defexception plug_status: 403, message: "You don't have permission to access this server"
end
