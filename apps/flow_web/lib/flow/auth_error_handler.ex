defmodule Flow.AuthErrorHandler do
  import Plug.Conn

  def auth_error(conn, {type, reason}, _opts) do
    case type do
      :unauthenticated ->
        Phoenix.Controller.redirect(conn, to: "/signin")

      :no_resource_found ->
        Phoenix.Controller.redirect(conn, to: "/signin")

      :already_authenticated ->
        Phoenix.Controller.redirect(conn, to: "/")

      _ ->
        conn
        |> put_status(401)
        |> Phoenix.Controller.render(FlowWeb.ErrorView, "401.html")
        |> halt
    end
  end
end
