defmodule Gaco.Plug do
  import Phoenix.Controller
  import Plug.Conn

  @doc """
  This plug function authorize an action on current controller for current_user.
  Return `conn` if authorized or return error page if not authorized


  **Example**
      plug authorize_role
  """
  def authorize_role(conn, _opts) do
    action = action_name(conn)
    current_user = conn.assigns[:current_user]
    do_authorize(conn, current_user, action, controller_module(conn))
  end

  @doc """
  This plug function call resource loader to load a resource using given criterial in connection params and put loaded resource in `assigns` with given key. If no resource found, return error page.


  **Options:**
  `model` required: resource model to load
  `param_key`: specify which key in param map should be used to load resource. Default to `:id`
  `key` required: assign loaded resource to conn using this key

  **Example**
      plug :load_resource, model: User, assign_key: :user, param_key: :user_id
  """
  def load_resource(conn, opts \\ []) do
    param_key = Keyword.get(opts, :param_key, :id) |> to_string
    assign_key = Keyword.get(opts, :key)
    model = Keyword.get(opts, :model)

    id = conn.params[param_key]

    if model && assign_key do
      do_load_resource(conn, model, id, assign_key)
    else
      raise ArgumentError, "Model name is missing"
    end
  end

  defp do_load_resource(conn, model, id, assign_key) do
    resource = Gaco.ResourceLoader.load(model, id)

    if resource do
      assign(conn, assign_key, resource)
    else
      raise Gaco.ResourceNotFound
    end
  end

  @doc """
  This plug function authorize an action on a given resource. This resouce must be in `conn.assigns` and identified by `resource_key`.
  Return `conn` if authorized or return error page if not authorized

  **Options:**
  `http_method_as_action`: if set to `true`, use HTTP method as action instead of using action name in `conn`. Default is `false`. HTTP actions are `:get, :put, :post, :delete, :patch`
  `key`: this key is used to get resouce from assigns



  **Example**
  plug :authorize_resource, key: :project, 
  """
  def authorize_resource(conn, opts \\ []) do
    use_http_action = Keyword.get(opts, :http_method_as_action, false)
    resource_key = Keyword.get(opts, :key)

    action =
      if use_http_action do
        conn.method
      else
        action_name(conn)
      end

    current_user = conn.assigns[:current_user]
    resource = conn.assigns[resource_key]

    do_authorize(conn, current_user, action, resource)
  end

  defp do_authorize(conn, user, action, resource) do
    cond do
      is_nil(resource) ->
        raise Gaco.ResourceNotFound

      user && Can.can?(user, action, resource) ->
        conn

      true ->
        raise Gaco.AccessDenied
    end
  end

  def load_resource_and_authorize(conn, opts \\ []) do
    load_resource(conn, opts)
    |> authorize_resource(opts)
  end
end
