defmodule FlowWeb.Plug.AssignResource do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, opts) do
    user = Flow.Guardian.Plug.current_resource(conn)

    conn
    |> assign(:current_user, user)
  end
end
