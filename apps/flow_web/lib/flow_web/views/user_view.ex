defmodule FlowWeb.UserView do
  use FlowWeb, :view
  alias Flow.Account.User

  def serialize(item, scope \\ :all, opts \\ [])

  def serialize(%User{} = item, :all, opts) do
    Map.take(item, [:id, :first_name, :last_name, :avatar, :username])
    |> Map.merge(%{
      full_name: full_name(item)
    })
  end

  def serialize(items, scope, opts) when is_list(items) do
    Enum.map(items, &serialize(&1, scope, opts))
  end

  def serialize(_item, _scope, _opts), do: nil
end
