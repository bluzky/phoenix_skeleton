defmodule FlowWeb.ViewHelpers do
  use Phoenix.HTML
  import FlowWeb.Gettext

  # template utils
  def render_share_view(template, assigns, opts \\ []) do
    assigns = Map.merge(assigns, Enum.into(opts, %{}))
    Phoenix.View.render(FlowWeb.ShareView, template, assigns)
  end

  def get_status_class(status, scope) do
    case scope do
      _ ->
        %{
          "info" => ["delivered", "closed", "assigned", "open"],
          "success" => [
            true,
            "active",
            "authorized",
            "completed"
          ],
          "warning" => ["in_progress", "in_review"],
          "danger" => [false, "inactive", "rejected"],
          "primary" => [],
          "secondary" => [],
          "dark" => ~w(archived draft),
          "light" => []
        }
        |> Enum.find(fn {k, v} -> status in v end)
    end
    |> Kernel.||({nil, nil})
    |> elem(0)
  end

  def status_badge(status, text \\ nil, scope \\ nil) do
    class = get_status_class(status, scope)
    text = text || humanize(status)

    raw("""
    <span class="badge badge-pill badge-#{class}">#{text}</span>
    """)
  end

  def status_text(status, text \\ nil, scope \\ nil) do
    class = get_status_class(status, scope)
    text = text || humanize(status)

    raw("""
    <i class="material-icons color-#{class}">fiber_manual_record</i> #{text}
    """)
  end

  # code only functions

  def format_date(date, format \\ "{0D}/{0M}/{YYYY}")

  def format_date(nil, _) do
    nil
  end

  def format_date(date, format) do
    Timex.format!(date, format)
  end

  @spec nav_state(Plug.Conn.t(), atom, list) :: any
  def nav_state(conn, controller, actions) when is_list(actions) do
    if conn.private.phoenix_controller == controller and conn.private.phoenix_action in actions do
      "active"
    end
  end

  @spec nav_state(Plug.Conn.t(), list) :: any
  def nav_state(conn, controllers) do
    if conn.private.phoenix_controller in controllers do
      "active"
    end
  end

  @doc """
  render list of option and mark selected option
  This is used only for select
  """
  def render_options(options, current_value) do
    options = [{gettext("All"), nil}] ++ options

    html =
      Enum.map(options, fn {label, value} ->
        selected = if to_string(value) == to_string(current_value), do: "selected"

        """
        <option value="#{value}" #{selected}>#{label}</option>
        """
      end)
      |> Enum.join("")

    raw(html)
  end

  def full_name(item) do
    "#{Map.get(item, :first_name)} #{Map.get(item, :last_name)}"
  end
end
