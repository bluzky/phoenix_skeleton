defmodule FlowWeb.CompanyView do
  use FlowWeb, :view
  alias Flow.Organization.Company

  def serialize(item, scope \\ :all, opts \\ [])

  def serialize(%Company{} = item, :all, opts) do
    Map.take(item, [:id, :name, :identifier, :logo])
    |> Map.merge(%{
      supported_by: FlowWeb.UserView.serialize(item.supported_by)
    })
  end

  def serialize(items, scope, opts) when is_list(items) do
    Enum.map(items, &serialize(&1, scope, opts))
  end

  def serialize(_item, _scope, _opts), do: nil

  def render("scripts.index.html", assigns) do
    """
    <script src="#{static_path(assigns.conn, "/js/company.js")}"></script>
    """
  end
end
