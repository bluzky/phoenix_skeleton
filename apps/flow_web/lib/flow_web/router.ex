defmodule FlowWeb.Router do
  use FlowWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :guardian do
    plug(
      Guardian.Plug.Pipeline,
      module: Flow.Guardian,
      error_handler: Flow.AuthErrorHandler
    )

    plug(Guardian.Plug.VerifySession)
  end

  pipeline :auth do
    plug(Guardian.Plug.EnsureAuthenticated)
    plug(Guardian.Plug.LoadResource)
    plug(FlowWeb.Plug.AssignResource)
  end

  pipeline :unauth do
    plug(:put_layout, {FlowWeb.LayoutView, :bare})
    plug(Guardian.Plug.EnsureNotAuthenticated)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", FlowWeb do
    pipe_through([:browser, :guardian, :unauth])

    get("/signin", SessionController, :new)
    post("/signin", SessionController, :create)
    get("/forgot_password", ProfileController, :forgot_password)
  end

  scope "/", FlowWeb do
    # Use the default browser stack
    pipe_through([:browser, :guardian, :auth])

    get("/", PageController, :index)
    get("/sign_out", SessionController, :delete)

    scope("/profile") do
      get("/", ProfileController, :profile)
      get("/edit", ProfileController, :edit_profile)
      put("/edit", ProfileController, :update_profile)
      get("/change_password", ProfileController, :change_password)
      put("/change_password", ProfileController, :update_password)
    end

    scope("/staffs") do
      put("/:id/block", UserController, :block)
      put("/:id/unblock", UserController, :unblock)
      resources("/", UserController)
    end

    scope("/settings") do
      get("/company_profile", CompanyController, :show)
      get("/company_profile/edit", CompanyController, :edit)
      get("/company_profile/update", CompanyController, :update)
    end

    scope("/departments") do
      put("/:id/block", DepartmentController, :block)
      put("/:id/unblock", DepartmentController, :unblock)
      resources("/", DepartmentController)
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", FlowWeb do
  #   pipe_through :api
  # end
end
