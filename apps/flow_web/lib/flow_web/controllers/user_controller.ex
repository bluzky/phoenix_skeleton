defmodule FlowWeb.UserController do
  use FlowWeb, :controller
  alias Flow.Account.User

  plug(:authorize_role)

  plug(
    :load_resource,
    [model: User, key: :user] when action in [:show, :edit, :update, :block, :unblock]
  )

  alias Flow.{Account}
  alias Flow.Account.User

  def index(conn, params) do
    user = conn.assigns[:current_user]
    paginator = Account.filter_user_paginate(params)

    entries = Repo.preload(paginator.entries, [:department])

    departments = Flow.Organization.filter_department(%{company_id: user.company_id})

    conn
    |> assign(:departments, departments)
    |> render("index.html", users: entries, paginator: %{paginator | entries: entries})
  end

  def new(conn, _params) do
    user = conn.assigns[:current_user]
    departments = Flow.Organization.filter_department(%{company_id: user.company_id})
    changeset = Account.change_user(%User{})

    conn
    |> assign(:departments, departments)
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    user = conn.assigns[:current_user]
    params = Map.put(user_params, "company_id", user.company_id)

    case Account.create_user(user_params) do
      {:ok, staff} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: user_path(conn, :show, staff))

      {:error, %Ecto.Changeset{} = changeset} ->
        user = conn.assigns[:current_user]
        departments = Flow.Organization.filter_department(%{company_id: user.company_id})

        conn
        |> assign(:departments, departments)
        |> render("new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = conn.assigns[:user]
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    admin = conn.assigns[:current_user]
    departments = Flow.Organization.filter_department(%{company_id: admin.company_id})

    staff = conn.assigns[:user]
    changeset = Account.change_user(staff)

    conn
    |> assign(:departments, departments)
    |> render("edit.html", user: staff, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    staff = conn.assigns[:user]

    case Account.update_user(staff, user_params) do
      {:ok, staff} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: user_path(conn, :show, staff))

      {:error, %Ecto.Changeset{} = changeset} ->
        admin = conn.assigns[:current_user]
        departments = Flow.Organization.filter_department(%{company_id: admin.company_id})

        conn
        |> assign(:departments, departments)
        |> render("edit.html", user: staff, changeset: changeset)
    end
  end

  def block(conn, %{"id" => id}) do
    do_update_status(conn, id, User.status_inactive())
  end

  def unblock(conn, %{"id" => id}) do
    do_update_status(conn, id, User.status_active())
  end

  defp do_update_status(conn, user_id, status) do
    user = conn.assigns[:user]

    case Account.update_user(user, %{status: status}) do
      {:ok, _user} ->
        conn
        |> put_flash(:success, "User account is updated successfully")
        |> redirect(to: user_path(conn, :index))

      {:error, _} ->
        conn
        |> put_flash(:error, "Cannot update user account")
        |> redirect(to: user_path(conn, :index))
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Account.get_user!(id)
    {:ok, _user} = Account.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: user_path(conn, :index))
  end
end
