defmodule FlowWeb.CompanyController do
  use FlowWeb, :controller

  alias Flow.Organization
  alias Flow.Organization.Company

  action_fallback(FlowWeb.FallbackController)

  plug(:authorize_role)

  def show(conn, _params) do
    user = conn.assigns[:current_user]
    company = user.company
    render(conn, "show.html", company: company)
  end

  def edit(conn, %{"id" => id}) do
    company = Organization.get_company!(id)
    changeset = Organization.change_company(company)
    render(conn, "edit.html", company: company, changeset: changeset)
  end

  def update(conn, %{"id" => id, "company" => company_params}) do
    company = Organization.get_company!(id)

    case Organization.update_company(company, company_params) do
      {:ok, company} ->
        conn
        |> put_flash(:info, "Company updated successfully.")
        |> redirect(to: company_path(conn, :show, company))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", company: company, changeset: changeset)
    end
  end

  def block(conn, %{"id" => id}) do
    do_update_status(conn, id, Company.status_inactive())
  end

  def unblock(conn, %{"id" => id}) do
    do_update_status(conn, id, Company.status_active())
  end

  defp do_update_status(conn, id, status) do
    company = Organization.get_company!(id)

    case Organization.update_company(company, %{status: status}) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Company is updated successfully")
        |> redirect(to: company_path(conn, :index))

      {:error, _} ->
        conn
        |> put_flash(:error, "Cannot update company")
        |> redirect(to: company_path(conn, :index))
    end
  end
end
