defmodule FlowWeb.FallbackController do
  @moduledoc false

  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use FlowWeb, :controller

  def call(conn, {:ok, data}) do
    json(conn, %{
      status: "OK",
      data: data
    })
  end

  def call(conn, {:error, :permission_denied}) do
    conn
    |> put_status(401)
    |> put_layout({FlowWeb.LayoutView, "error.html"})
    |> render(FlowWeb.ErrorView, "401.html")
    |> halt()
  end

  def call(conn, {:error, message}) when is_binary(message) do
    json(conn, %{
      status: "ERROR",
      message: message
    })
  end
end
