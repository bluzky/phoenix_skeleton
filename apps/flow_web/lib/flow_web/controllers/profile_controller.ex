defmodule FlowWeb.ProfileController do
  use FlowWeb, :controller

  alias Flow.Account

  def profile(conn, _) do
    user = conn.assigns[:current_user]
    render(conn, "show.html", user: user)
  end

  def edit_profile(conn, _) do
    user = conn.assigns[:current_user]
    changeset = Account.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update_profile(conn, %{"user" => user_params}) do
    user = conn.assigns[:current_user]

    case Account.update_profile(user, user_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Updated profile successfully.")
        |> redirect(to: user_path(conn, :profile))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def change_password(conn, _) do
    user = conn.assigns[:current_user]
    changeset = Account.change_user(user)

    render(conn, "change_password.html", changeset: changeset)
  end

  def update_password(conn, %{"user" => params}) do
    user = conn.assigns[:current_user]

    case Account.change_password(user, params) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Updated password successfully.")
        |> redirect(to: profile_path(conn, :profile))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "change_password.html", user: user, changeset: changeset)
    end
  end

  def forgot_password(conn, _) do
    render(conn, "forgot_password.html")
  end
end
