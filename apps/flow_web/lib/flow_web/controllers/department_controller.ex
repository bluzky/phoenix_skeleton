defmodule FlowWeb.DepartmentController do
  use FlowWeb, :controller

  alias Flow.Organization
  alias Flow.Filter
  alias Flow.Organization.Department

  def index(conn, params) do
    user = conn.assigns[:current_user]

    filters =
      Filter.parse(
        [{:keyword, :string, nil}, {:status, :string, nil}],
        params
      )
      |> Map.put(:company_id, user.company_id)

    paginator = Organization.filter_department_paginate(filters, params)

    conn
    |> assign(:filters, filters)
    |> render("index.html", departments: paginator.entries, paginator: paginator)
  end

  def new(conn, _params) do
    changeset = Organization.change_department(%Department{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"department" => department_params}) do
    user = conn.assigns[:current_user]

    case Organization.create_department(user.company, department_params) do
      {:ok, department} ->
        conn
        |> put_flash(:info, "Department created successfully.")
        |> redirect(to: department_path(conn, :show, department))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    department = Organization.get_department!(id)
    render(conn, "show.html", department: department)
  end

  def edit(conn, %{"id" => id}) do
    department = Organization.get_department!(id)
    changeset = Organization.change_department(department)
    render(conn, "edit.html", department: department, changeset: changeset)
  end

  def update(conn, %{"id" => id, "department" => department_params}) do
    department = Organization.get_department!(id)

    case Organization.update_department(department, department_params) do
      {:ok, department} ->
        conn
        |> put_flash(:info, "Department updated successfully.")
        |> redirect(to: department_path(conn, :show, department))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", department: department, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    department = Organization.get_department!(id)
    {:ok, _department} = Organization.delete_department(department)

    conn
    |> put_flash(:info, "Department deleted successfully.")
    |> redirect(to: department_path(conn, :index, department.company_id))
  end

  def block(conn, %{"id" => id}) do
    do_update_status(conn, id, Department.status_inactive())
  end

  def unblock(conn, %{"id" => id}) do
    do_update_status(conn, id, Department.status_active())
  end

  defp do_update_status(conn, id, status) do
    department = Organization.get_department!(id)

    case Organization.update_department(department, %{status: status}) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Department is updated successfully")
        |> redirect(to: department_path(conn, :index, department.company_id))

      {:error, _} ->
        conn
        |> put_flash(:error, "Cannot update department")
        |> redirect(to: department_path(conn, :index, department.company_id))
    end
  end
end
