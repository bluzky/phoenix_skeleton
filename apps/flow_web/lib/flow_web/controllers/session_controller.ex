defmodule FlowWeb.SessionController do
  use FlowWeb, :controller
  alias Flow.Account.User
  alias Flow.Account

  defp login_changeset(params \\ %{}) do
    data = %{}
    types = %{email: :string, password: :string}

    {data, types}
    |> Ecto.Changeset.cast(params, Map.keys(types))
    |> Ecto.Changeset.validate_required([:email, :password])
  end

  def new(conn, _) do
    render(conn, "new.html", changeset: login_changeset())
  end

  def create(conn, %{"login" => params}) do
    changeset = login_changeset(params)

    if changeset.valid? do
      data = Ecto.Changeset.apply_changes(changeset)
      user = Account.find_user_one(%{email: data.email, status: User.status_active()})

      with false <- is_nil(user),
           {:ok, _} <- Comeonin.Bcrypt.check_pass(user, data.password, hash_key: :password) do
        Flow.Guardian.Plug.sign_in(conn, user)
        |> redirect(to: page_path(conn, :index))
      else
        _ ->
          conn
          |> put_flash(:error, gettext("Email or password is incorrect"))
          |> render("new.html", changeset: changeset)
      end
    else
      render(conn, "new.html", changeset: %{changeset | action: :validate})
    end
  end

  def delete(conn, _params) do
    Flow.Guardian.Plug.sign_out(conn)
    |> redirect(to: session_path(conn, :new))
  end
end
