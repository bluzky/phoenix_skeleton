// import AdminApi from '../services/admin_api.js';

import('../services/company_api.js').then(module => {

  const CompanyApi = module.default;

  $(document).ready(function(){
    $(".js-assign").click(function(e){
      e.preventDefault();
      let companyId = $(this).data("company-id");

      loadCompany(companyId);
      window.$("#assign-modal").modal("show");
    });
  });

  function loadCompany(id){
    CompanyApi.getCompany(id).then( resp => {
      if(resp.status == "OK"){
        const data = resp.data;
        
        window.mapObjectDom({
          "v-company-id value": data.id,
          "v-company-name": data.name,
          "v-company-identifier": "@" + data.identifier,
          "v-company-logo src": data.logo,
          "v-supporter-name": getIn(data, ["supported_by", "full_name"])
        });

      }else{
        window.showToast("error", "Cannot connect to server");
      }
    });
  }

  function getIn(object, path){
    let obj = object;

    for(var i = 0; i< path.length && obj != null ; i++){
      obj = obj[path[i]];
    }
    return obj;
  }
});

