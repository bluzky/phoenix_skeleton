import axios from 'axios';

const csrf = document.querySelector('meta[name=csrf]').attributes.content.value;
axios.defaults.baseURL = '/api';
axios.defaults.headers.post['x-csrf-token'] = csrf;


export default {
  getCompany: function(id){
    return axios.get('/companies/' + id)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        window.showToast("Cannot connect to server");
      });
  }
};


