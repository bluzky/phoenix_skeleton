import axios from 'axios';

const csrf = document.querySelector('meta[name=csrf]').attributes.content.value;
axios.defaults.baseURL = '/api';
axios.defaults.headers.post['x-csrf-token'] = csrf;


export default {
  listStaff: function(){
    // Make a request for a user with a given ID
    return axios.get('/admin/list_staff')
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        window.showToast("Cannot connect to server");
      })
  }
};


