
window.showToast = function(type, message) {
    var x = document.getElementById("snackbar");

    var node = document.createElement("div");

    // Add the "show" class to DIV
    node.className = type + " snack-item";
    node.innerHTML = message;
    x.appendChild(node);

    // After 3 seconds, remove the show class from DIV
    node.className += " show";
    setTimeout(function() {
        x.removeChild(node);
    }, 3000);
}

window.request = function(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            let value = params[key];

            if (value.constructor == Array) {
                for (let i = 0; i < value.length; i++) {
                    let hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key + "[]");
                    hiddenField.setAttribute("value", value[i]);
                    form.appendChild(hiddenField);
                }
            } else if (value.constructor == Object) {
                for (let subkey in value) {
                    let hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", `${key}[${subkey}]`);
                    hiddenField.setAttribute("value", value[subkey]);
                    form.appendChild(hiddenField);
                }
            } else {
                let hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
            }
        }
    }

    document.body.appendChild(form);
    form.submit();
};

window.mapValueDom = function(id, value, scope){
  var selector, attribute;
  [selector, attribute] = id.replace(/\s+/g, " ").split(" ");
  attribute = attribute || "innerHTML";

  scope = scope || document;
  let el = scope.querySelector("." + selector);

  if(el && value){
    if(attribute == "innerHTML")
      el[attribute] = value;
    else
      el.setAttribute(attribute, value);
  }
};

window.mapObjectDom = function(map, scope){
  scope = scope ? document.querySelector(scope) : document;
  if(scope){
      for(var k in map){
        mapValueDom(k, map[k], scope);
      }
  }
};
