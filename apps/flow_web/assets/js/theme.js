/*
 * Name:        Oscar
 * Written by: 	Unifato - (http://unifato.com)
 * Version:     1.0.0
 */

// import * as $ from 'jquery';
import ('jquery').then(module => {

    var $ = window.$ = module.default;

    var initBodyClass = "";
    if ($("body").hasClass("sidebar-collapse")) {
        initBodyClass = "sidebar-collapse";
    } else if ($("body").hasClass("sidebar-expand")) {
        initBodyClass = "sidebar-expand";
    }

    var Unifato = {
        init: function() {
            this.collapseNavbar();
            $(window).scroll(this.collapseNavbar);
            this.contentHeight();

            this.header();
            this.sidebar();
            this.content();
            this.enableScrollbar();
          this.inputFocus();
          this.enablePlugins();
        },

        collapseNavbar: function() {
            if ($(window).scrollTop() > 30) {
                $("#wrapper").addClass("fix-top");
            } else {
                $("#wrapper").removeClass("fix-top");
            }
        },

        header: function() {

        },

        contentHeight: function() {
            var width = window.innerWidth > 0 ? window.innerWidth : screen.width;
            var l;

            if (width > 720 && $("body").hasClass("collapse-collapse")) {
                l = $(".site-sidebar").outerHeight() - 1;
                $(".main-wrapper").css("min-height", l + "px");
            }
            if (width > 720 && $("body").hasClass('sidebar-horizontal')) {
                l = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 1 - $(".navbar").outerHeight() - $('.site-sidebar').outerHeight();
                $(".main-wrapper").css("min-height", l + "px");
            } else if (width > 720) {
                l = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 1 - $(".navbar").outerHeight();
                $(".main-wrapper").css("min-height", l + "px");
            }
        },

        content: function() {
            $(".card-expandable .card-header").on("click", function(e) {
                $(this).parents(".card").toggleClass("card-expanded");
            });

            this.enableMagnificPopup();

            var scrollToBottom = document.getElementsByClassName('scroll-to-bottom');
            for (var i = 0; i < scrollToBottom.length; i++) {
                scrollToBottom[i].scrollTop = scrollToBottom[i].scrollHeight;
            }

            $('.blog-post-share-links .toggle-link').click(function() {
                $(this).closest('ul').toggleClass('active');
            });
        },

        initScrollbar: function($el) {
            $el.perfectScrollbar($el[0].dataset);
        },

        sidebar: function() {
            var self = this;
            if (document.body.classList.contains('sidebar-expand'))
                // $('.side-menu').metisMenu({ preventDefault: true });
                // $('.side-menu').on('show.metisMenu',function() {
                //   $('.site-sidebar.scrollbar-enabled').perfectScrollbar('destroy');
                //   self.initScrollbar( $('.site-sidebar.scrollbar-enabled') );
                // });
                this.sidebarToggle();
            this.rightSidebarToggle();
            this.sidebarUserToggle();
            this.chatSidebar();
        },

        setMenu: function() {
            var self = this;
            var width = window.innerWidth > 0 ? window.innerWidth : screen.width;
            var $body = $("body");
            $('.site-sidebar.scrollbar-enabled').perfectScrollbar('destroy').removeClass('ps');

            if (width < 961) {
              // $(".site-sidebar").hide();
            }
            else if (width > 960 && width < 1170 && initBodyClass == "sidebar-expand") {
                $(".site-sidebar").show();
                $body.removeClass("sidebar-expand");
                $body.addClass("sidebar-collapse");
            } else if (width >= 1170 && initBodyClass == "sidebar-expand") {
                $(".site-sidebar").show();
                $body.removeClass("sidebar-collapse");
                $body.addClass("sidebar-expand");
                Unifato.initScrollbar($('.site-sidebar.scrollbar-enabled'));
            } else if ($body.hasClass("sidebar-expand")) {
                $(".site-sidebar").show();
                Unifato.initScrollbar($('.site-sidebar.scrollbar-enabled'));
            } else if (document.body.classList.contains('sidebar-horizontal')) {
                $(".site-sidebar").show();
                // $('.side-menu').metisMenu('dispose');
            } else {
                $(".site-sidebar").show();
            }
        },

        sidebarToggle: function() {
            var self = this;
            $(".sidebar-toggle a").on("click", function() {
                var width = window.innerWidth > 0 ? window.innerWidth : screen.width;
                var $body = $("body");
                $('.site-sidebar.scrollbar-enabled').perfectScrollbar('destroy').removeClass('ps');

                if (width < 961) {
                    $(".site-sidebar").toggle();
                } else if ($body.hasClass("sidebar-expand")) {
                    $body.removeClass("sidebar-expand");
                    $body.addClass("sidebar-collapse");
                    $(".side-user > a").removeClass("active");
                    $(".side-user > a").siblings(".side-menu").hide();
                    $('.side-menu .sub-menu').css('height', 'auto');
                    // $('.side-menu').metisMenu('dispose');
                } else if ($body.hasClass("sidebar-collapse")) {
                    $body.removeClass("sidebar-collapse");
                    $body.addClass("sidebar-expand");
                    self.initScrollbar($('.site-sidebar.scrollbar-enabled'));
                    // $('.side-menu').metisMenu({ preventDefault: true });
                }

                Unifato.contentHeight();
                if (width > 961) {
                    $(document).trigger("SIDEBAR_CHANGED_WIDTH");
                }
            });

        },

        rightSidebarToggle: function() {
            $(".right-sidebar-toggle").on("click", function(e) {
                $('.dropdown-toggle[aria-expanded="true"]').dropdown('toggle');
                $('body').toggleClass('right-sidebar-expand');
                return false;
            });

            // document.addEventListener('click', function(event) {
            //     var $rightSidebar = document.getElementsByClassName('right-sidebar')[0],
            //         $chatPanel = document.getElementsByClassName('chat-panel')[0];
            //     var isInsideContainer = $rightSidebar.contains(event.target) || $chatPanel.contains(event.target);
            //     if (!isInsideContainer) {
            //         document.body.classList.remove('right-sidebar-expand');
            //         $chatPanel.hidden = 'hidden';
            //     }
            // });
        },

        sidebarUserToggle: function() {
            $(".side-user > a").on("click", function() {
                if ($('body').hasClass("sidebar-collapse") === false) {
                    $(this).toggleClass("active");
                    $(this).siblings(".side-menu").toggle();
                }
            });
        },

        chatSidebar: function() {
            var el = $('[data-plugin="chat-sidebar"]');
            if (!el.length) return;
            var chatList = el.find('.chat-list');
            this.chatPanel();
            chatList.each(function(index) {
                var $this = $(this);
                $(this).find('.list-group a').on('click', function() {
                    $this.find('.list-group a.active').removeClass('active');
                    $(this).addClass('active');
                    var el = $('.chat-panel');
                    if (!el.length) return;
                    el.removeAttr('hidden');

                    // For Scroll to Bottom
                    var messages = el.find('.messages');
                    messages[0].scrollTop = messages[0].scrollHeight;
                    if (messages[0].classList.contains('scrollbar-enabled')) {
                        messages.perfectScrollbar('update');
                    }

                    el.find('.user-name').html($(this).data('chat-user'));
                });
            });
        },

        chatPanel: function() {
            var el = $('.chat-panel');
            if (!el.length) return;

            // Close Button
            el.find('.close').on('click', function() {
                el.attr('hidden', true);
                el.find('.panel-body').removeClass('hide');
            });

            // Minimize Button
            el.find('.minimize').on('click', function() {
                el.find('.card-block').attr('hidden', !el.find('.card-block').attr('hidden'));
                if (el.find('.card-block').attr('hidden') === 'hidden')
                    $(this).find('.material-icons').html('expand_less');
                else
                    $(this).find('.material-icons').html('expand_more');
            });

        },

        enableMagnificPopup: function() {
            var el = $('[data-toggle="lightbox"], [data-toggle="lightbox-gallery"]');
            if (!el.length) return;
            el.each(function() {
                var $this = $(this),
                    src = $this.data('src'),
                    type = $this.data('type'),
                    defaults = {},
                    options = $this.data('plugin-options') !== undefined ? $this.data('plugin-options') : {},
                    lightboxClass = $this.data('lightbox-class') !== undefined ? $this.data('lightbox-class') : "";

                if ($this.data('toggle') === "lightbox") {
                    defaults = {
                        type: type,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated ' + this.st.el.attr('data-effect'));
                            }
                        }
                    };
                    options = $.extend({}, defaults, options);
                    $this.magnificPopup(options);
                } else if ($this.data('toggle') === "lightbox-gallery") {
                    defaults = {
                        type: type,
                        delegate: 'a',
                        gallery: {
                            enabled: true,
                        },
                        callback: {
                            beforeOpen: function() {
                                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated ' + this.st.el.attr('data-effect'));
                            }
                        }
                    };
                    options = $.extend({}, defaults, options);
                    $this.magnificPopup(options);
                }
            });
        },

        enableScrollbar: function() {
            var el = $('.scrollbar-enabled, .dropdown-list-group ');
            if (!el.length) return;
            el.each(function(index) {

                var $this = $(this);
                var options = {
                    wheelSpeed: 0.5
                };
                if (this.classList.contains('suppress-x')) options.suppressScrollX = true;
                if (this.classList.contains('suppress-y')) options.suppressScrollY = true;
                $this.perfectScrollbar(options);

                if ($this.parent().parent().hasClass('dropdown-card')) {
                    $(document).on('shown.bs.dropdown', $this.parent().parent(), function() {
                        $this.perfectScrollbar('update');
                    });
                }

                // $(document).on('show.metisMenu, hide.metisMenu', function() {
                //   $this.perfectScrollbar('update');
                // });

                if (this.classList.contains('scroll-to-bottom')) {
                    this.scrollTop = this.scrollHeight;
                    $this.perfectScrollbar('update');
                }

            });
        },

        inputFocus: function() {
            var el = $('input:not([type=checkbox]):not([type=radio]), textarea');
            if (!el.length) return;

            el.each(function() {
                var $this = $(this),
                    self = this;

                var hasValueFunction = function() {
                    if (self.value.length > 0) {
                        self.parentNode.classList.add('input-has-value');
                        $(self).closest('.form-group').addClass('input-has-value');
                    } else {
                        self.parentNode.classList.remove('input-has-value');
                        $(self).closest('.form-group').removeClass('input-has-value');
                    }
                };

                hasValueFunction(this);
                $this.on('input', hasValueFunction);

                $this.focusin(function() {
                    this.parentNode.classList.add('input-focused');
                    $this.closest('.form-group').addClass('input-focused');
                });
                $this.focusout(function() {
                    this.parentNode.classList.remove('input-focused');
                    $this.closest('.form-group').removeClass('input-focused');
                });

                $this.find('.remove-focus').on('click', function() {
                    $this.emit('focusout');
                });
            });
        },

      enablePlugins: function(){
        this.enableDatePicker();
        this.enableSelect2();
      },
      enableDatePicker: function() {
        import('bootstrap-datepicker').then(mod => {
          var el = $('.datepicker');
          if( !el.length ) return;
          var defaults = {
          };
          el.each(function(){
            var $this = $(this),
                options = $this.data('plugin-options');

            if( this.unifato === undefined )
              this.unifato = {};

            if( options === undefined ) options = {};
            options = $.extend({}, defaults, options);
            $this.datepicker(options);
            this.unifato.datepicker = $this.data('datepicker');
          });
        })
      },

      enableSelect2: function() {
        import('select2').then(mod => {
          var el = $('[data-toggle="select2"]');
          if(!el.length) return;
          el.each( function() {
            var $this = $(this),
                options = $this.data('plugin-options');

            if( this.unifato === undefined )
              this.unifato = {};

            $this.select2(options);
            this.unifato.select2 = $this.data('select2');
          });
        });
      },
    };


    Unifato.init();
    Unifato.setMenu();


    window.addEventListener('resize', Unifato.setMenu);
    window.addEventListener('resize', Unifato.contentHeight);
});
