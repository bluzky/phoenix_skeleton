defmodule Flow do
  @moduledoc """
  Flow keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def model do
    quote do
      use Ecto.Schema
      import Flow.EctoUtils
      import Ecto.Changeset
      import Flow.Gettext
    end
  end

  def business do
    quote do
      alias Flow.Repo
      import Ecto.Changeset
      import Ecto.Query
      import Flow.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
