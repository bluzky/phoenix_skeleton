defmodule Flow.Account.User do
  use Flow, :model

  def_enum("status", ~w(active inactive))
  def_enum("role", ~w(manager staff))

  def role_input_enum() do
    [
      {gettext("Human resource"), "hr"},
      {gettext("Sale staff"), "sale_staff"},
      {gettext("Product staff"), "product_staff"},
      {gettext("Inventory staff"), "inventory_staff"},
      {gettext("Sale manager"), "sale_manager"},
      {gettext("Product manager"), "product_manager"},
      {gettext("Inventory manager"), "inventory"}
    ]
  end

  def status_input_enum() do
    [
      {gettext("Active"), "active"},
      {gettext("Inactive"), "inactive"}
    ]
  end

  schema "users" do
    field(:first_name, :string)
    field(:last_name, :string)
    field(:full_name, :string)
    field(:username, :string)
    field(:email, :string)
    field(:password, :string)
    field(:phone, :string)
    field(:avatar, :string)
    field(:title, :string)

    field(:roles, {:array, :string}, default: ["staff"])

    belongs_to(:company, Flow.Organization.Company)
    belongs_to(:department, Flow.Organization.Department)

    field(:reset_password_token, :string)
    field(:status, :string, default: "active")

    field(:password_confirmation, :string, virtual: true)
    field(:current_password, :string, virtual: true)
    timestamps()
  end

  @required_fields ~w(first_name last_name username email roles)a
  @optional_fields ~w(title phone avatar company_id department_id password reset_password_token status)a
  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> unique_constraint(:username)
    |> unique_constraint(:email)
    |> hash_pw()
    |> build_fullname()
  end

  def profile_changeset(user, attrs) do
    user
    |> cast(attrs, ~w(first_name last_name username phone avatar dob address)a)
    |> cast_date(attrs, :dob, "{0D}/{0M}/{YYYY}")
    |> cast_date(attrs, :start_date, "{0D}/{0M}/{YYYY}")
    |> validate_required(~w(first_name last_name username)a)
    |> unique_constraint(:username)
  end

  def changepw_changeset(user, attrs) do
    user
    |> cast(attrs, [:password, :password_confirmation, :current_password])
    |> validate_required([:password, :password_confirmation, :current_password])
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> hash_pw()
  end

  defp hash_pw(changeset) do
    password = get_change(changeset, :password)

    if password do
      hash = Comeonin.Bcrypt.hashpwsalt(password)
      put_change(changeset, :password, hash)
    else
      changeset
    end
  end

  defp build_fullname(changeset) do
    first_name = get_field(changeset, :first_name)
    last_name = get_field(changeset, :last_name)
    full_name = "#{first_name} #{last_name}"

    put_change(changeset, :full_name, full_name)
  end
end
