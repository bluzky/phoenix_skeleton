defmodule Flow.Account do
  @moduledoc """
  The Account context.
  """
  use Flow, :business
  alias Flow.Account.User
  alias Flow.{Filter, Paginator}

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)
  def get_user(id), do: Repo.get(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  User update his/her profile. There are only some fields is allowed to update
  ## Examples

  iex> update_profile(user, %{field: new_value})
  {:ok, %User{}}

  iex> update_profile(user, %{field: bad_value})
  {:error, %Ecto.Changeset{}}

  """
  def update_profile(%User{} = user, attrs) do
    user
    |> User.profile_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  User update his/her account password
  ## Examples

  iex> change_password(user, %{password: new_value})
  {:ok, %User{}}

  iex> change_password(user, %{field: bad_value})
  {:error, %Ecto.Changeset{}}

  """
  def change_password(%User{} = user, attrs) do
    changeset = User.changepw_changeset(user, attrs)
    current_password = Ecto.Changeset.get_change(changeset, :current_password)

    if changeset.valid? do
      # check current password
      case Comeonin.Bcrypt.check_pass(user, current_password, hash_key: :password) do
        {:ok, _} ->
          Repo.update(changeset)

        _ ->
          changeset =
            Ecto.Changeset.add_error(
              changeset,
              :current_password,
              dgettext("errors", "Current password is not correct")
            )

          {:error, %{changeset | action: :validate}}
      end
    else
      {:error, %{changeset | action: :validate}}
    end
  end

  def find_user_one(filters) do
    from(u in User)
    |> Filter.apply(filters)
    |> first()
    |> Repo.one()
  end

  def filter_user(filters) do
    from(u in User)
    |> Filter.apply(filters)
    |> Repo.all()
  end

  def filter_user_paginate(params \\ %{}) do
    filters =
      Filter.parse(
        [
          {:keyword, :string, nil},
          {:status, :string, nil},
          {:roles, :string, nil},
          {:department_id, :integer, nil}
        ],
        params
      )

    from(u in User)
    |> Filter.apply(Map.take(filters, [:status, :department_id]))
    |> Filter.search_array(:roles, filters.roles)
    |> Filter.search([:first_name, :last_name, :email, :phone], filters[:keyword])
    |> order_by(asc: :first_name)
    |> Paginator.new(params)
  end
end
