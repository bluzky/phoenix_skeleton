defmodule Flow.Organization do
  @moduledoc """
  The Organization context.
  """

  import Ecto.Query, warn: false
  alias Flow.Repo

  alias Flow.Organization.Company
  alias Elixir.Flow.{Filter, Paginator}

  @doc """
  Filter company using given criterials and return the list of companies.

  ## Examples

      iex> filter_companies(%{name: "abc"})
      [%Company{}, ...]

  """
  def find_first_company(filters \\ %{}) do
    from(u in Company)
    |> Filter.apply(filters)
    |> first()
    |> Repo.one()
  end

  def filter_company(filters \\ %{}) do
    from(u in Company)
    |> Filter.apply(filters)
    |> Repo.all()
  end

  def filter_company_paginate(filters, params \\ %{}) do
    from(u in Company)
    |> Filter.apply(filters)
    |> Filter.search([:name, :identifier], filters[:keyword])
    |> order_by(asc: :name)
    |> Paginator.new(params)
  end

  @doc """
  Gets a single company.

  Raises `Ecto.NoResultsError` if the Company does not exist.

  ## Examples

      iex> get_company!(123)
      %Company{}

      iex> get_company!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company!(id), do: Repo.get!(Company, id)

  @doc """
  Creates a company.

  ## Examples

      iex> create_company(%{field: value})
      {:ok, %Company{}}

      iex> create_company(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company(attrs \\ %{}) do
    %Company{}
    |> Company.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a company.

  ## Examples

      iex> update_company(company, %{field: new_value})
      {:ok, %Company{}}

      iex> update_company(company, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company(%Company{} = company, attrs) do
    company
    |> Company.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Company.

  ## Examples

      iex> delete_company(company)
      {:ok, %Company{}}

      iex> delete_company(company)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company(%Company{} = company) do
    Repo.delete(company)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company changes.

  ## Examples

      iex> change_company(company)
      %Ecto.Changeset{source: %Company{}}

  """
  def change_company(%Company{} = company) do
    Company.changeset(company, %{})
  end

  alias Flow.Organization.Department
  alias Elixir.Flow.{Filter, Paginator}

  @doc """
  Filter department using given criterials and return the list of departments.

  ## Examples

      iex> filter_departments(%{name: "abc"})
      [%Department{}, ...]

  """
  def find_first_department(filters \\ %{}) do
    from(u in Department)
    |> Filter.apply(filters)
    |> first()
    |> Repo.one()
  end

  def filter_department(filters \\ %{}) do
    from(u in Department)
    |> Filter.apply(filters)
    |> Repo.all()
  end

  def filter_department_paginate(filters, params \\ %{}) do
    from(u in Department)
    |> Filter.apply(filters)
    |> Filter.search([:name], filters[:keyword])
    |> Paginator.new(params)
  end

  @doc """
  Gets a single department.

  Raises `Ecto.NoResultsError` if the Department does not exist.

  ## Examples

      iex> get_department!(123)
      %Department{}

      iex> get_department!(456)
      ** (Ecto.NoResultsError)

  """
  def get_department!(id), do: Repo.get!(Department, id)

  @doc """
  Creates a department.

  ## Examples

      iex> create_department(%{field: value})
      {:ok, %Department{}}

      iex> create_department(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_department(%Company{id: id}, attrs \\ %{}) do
    %Department{company_id: id}
    |> Department.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a department.

  ## Examples

      iex> update_department(department, %{field: new_value})
      {:ok, %Department{}}

      iex> update_department(department, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_department(%Department{} = department, attrs) do
    department
    |> Department.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Department.

  ## Examples

      iex> delete_department(department)
      {:ok, %Department{}}

      iex> delete_department(department)
      {:error, %Ecto.Changeset{}}

  """
  def delete_department(%Department{} = department) do
    Repo.delete(department)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking department changes.

  ## Examples

      iex> change_department(department)
      %Ecto.Changeset{source: %Department{}}

  """
  def change_department(%Department{} = department) do
    Department.changeset(department, %{})
  end
end
