defmodule Flow.Organization.Company do
  use Flow, :model

  def_enum("status", ~w(active inactive))

  def status_input_enum() do
    [
      {gettext("Active"), "active"},
      {gettext("Inactive"), "inactive"}
    ]
  end

  schema "companies" do
    field(:name, :string)
    field(:identifier, :string)
    field(:logo, :string)
    field(:email, :string)
    field(:phone, :string)
    field(:website, :string)
    field(:status, :string, default: "active")

    timestamps()
  end

  @required_fields [:name, :identifier]
  @optional_fields [:logo, :email, :phone, :website, :status]

  @doc false
  def changeset(company, attrs) do
    company
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:identifier)
  end
end
