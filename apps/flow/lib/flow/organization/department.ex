defmodule Flow.Organization.Department do
  use Flow, :model

  def_enum("status", ~w(active inactive))

  def status_input_enum() do
    [
      {gettext("Active"), "active"},
      {gettext("Inactive"), "inactive"}
    ]
  end

  schema "departments" do
    field(:name, :string)
    field(:status, :string, default: "active")
    belongs_to(:company, Flow.Organization.Company)

    timestamps()
  end

  @required_fields [:name, :company_id, :status]
  @optional_fields []

  @doc false
  def changeset(department, attrs) do
    department
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
