defmodule Flow.Filter do
  alias Share.Errors.BadRequestError

  @moduledoc """
  Filters Ecto query results by params provided.

  Author: Vince Urag
  www.codesforbreakfast.com
  """

  import Ecto.Query
  alias Ecto.Changeset

  @doc """
  schema is a list of tuple {name, type, default_value}
  """
  def parse(schema, params) when is_list(schema) do
    types =
      Enum.map(schema, fn {name, type, _} -> {name, type} end)
      |> Enum.into(%{})

    data =
      Enum.map(schema, fn {name, _, data} -> {name, data} end)
      |> Enum.into(%{})

    Changeset.cast({data, types}, params, Map.keys(types))
    |> Changeset.apply_changes()
  end

  @doc """
  Apply filter on multiple column

  if value is an array, filter will apply or condition. To use and condition, use `filter_and` instead
  """
  @spec apply(Ecto.Query.t(), keyword() | map()) :: Ecto.Query.t()
  def apply(query, filters) when is_list(filters) or is_map(filters) do
    filters =
      Enum.into(filters, %{})
      |> Map.delete(:keyword)

    Enum.reduce(filters, query, fn {key, val}, acc ->
      filter(acc, key, val)
    end)
  end

  @doc """
  Apply filter on single column

  If filter value is list, filter row that match any value in the list
  """
  @spec filter(Ecto.Query.t(), atom, list) :: Ecto.Query.t()
  def filter(query, _, values) when values in [[], "", nil, %{}], do: query

  def filter(query, fieldname, values) when is_list(values) do
    dynamic_query = dynamic([q], field(q, ^fieldname) in ^values)

    query |> where(^dynamic_query)
  end

  def filter(query, fieldname, value) do
    dynamic_query = dynamic([q], field(q, ^fieldname) == ^value)
    query |> where(^dynamic_query)
  end

  @doc """
  filter array columns which contains one of input value
  """
  def search_array(query, _fieldname, value) when value in [[], nil, ""], do: query

  def search_array(query, fieldname, values) when is_list(values) do
    dynamic_query =
      Enum.reduce(values, false, fn value, d_query ->
        dynamic([q], ^value in field(q, ^fieldname) or ^d_query)
      end)

    query |> where(^dynamic_query)
  end

  def search_array(query, fieldname, value) do
    search_array(query, fieldname, [value])
  end

  @doc """
  Search text on multiple column 
  """
  def search(query, _columns, value) when value in ["", nil], do: query

  def search(query, columns, value) when is_list(columns) do
    IO.inspect(columns)
    IO.inspect(value)
    search_str = "%#{value}%"

    dynamic_query =
      Enum.reduce(columns, false, fn fieldname, d_query ->
        dynamic([q], ilike(field(q, ^fieldname), ^search_str) or ^d_query)
      end)

    query |> where(^dynamic_query)
  end

  def search(query, fieldname, value) do
    search(query, [fieldname], value)
  end

  def ft_search(query, _, text) when text in [nil, ""], do: query

  def ft_search(query, fieldname, text) do
    dynamic_query = dynamic([q], fragment("? @@ to_tsquery(?)", field(q, ^fieldname), ^text))
    query |> where(^dynamic_query)
  end
end
