defmodule Flow.Loadmore do
  import Ecto.Query
  alias Flow.Repo

  @default_values %{limit: 20, max_id: nil}
  def default_values, do: @default_values

  @default_types %{
    limit: :integer,
    max_id: :integer
  }

  defp parse(params) do
    {@default_values, @default_types}
    |> Ecto.Changeset.cast(params, Map.keys(@default_values))
    |> Ecto.Changeset.apply_changes()
  end

  @doc """
  Load more query with give paging param object
  """
  def new(query, params) do
    params = parse(params)

    query =
      if is_nil(params.max_id) do
        query
      else
        where(query, [i], i.id < ^params.max_id)
      end

    entries =
      query
      |> limit(^params.limit)
      |> Repo.all()

    %{
      entries: entries,
      next_params: build_next_params(entries, params)
    }
  end

  defp build_next_params(entries, params) do
    ids = Enum.map(entries, & &1.id)

    if length(ids) == params.limit do
      max_id = Enum.min(ids)
      %{limit: params.limit, max_id: max_id}
    end
  end
end
