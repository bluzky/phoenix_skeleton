defmodule Flow.EctoUtils do
  import Ecto.Changeset
  require Logger
  import Flow.Gettext

  defmacro def_enum(prefix, values) do
    quote bind_quoted: [prefix: prefix, values: values] do
      def unquote(:"#{prefix}_enum")(), do: unquote(values)

      Enum.map(values, fn value ->
        def unquote(:"#{prefix}_#{value}")(), do: unquote(value)
      end)
    end
  end

  def require_one(changeset, keys) do
    valid =
      Enum.reduce(keys, false, fn key, acc ->
        value = get_field(changeset, key)

        if value not in [nil, "", [], %{}] do
          true
        else
          false or acc
        end
      end)

    if not valid do
      Enum.reduce(keys, changeset, fn key, acc ->
        add_error(
          acc,
          key,
          dgettext(
            "errors",
            "At least one of these keys %{keys} must be set",
            keys: inspect(keys)
          )
        )
      end)
    else
      changeset
    end
  end

  def require_not_or_both(changeset, key1, key2) do
    value1 = get_field(changeset, key1)
    value2 = get_field(changeset, key2)

    if (is_nil(value1) and is_nil(value2)) or (not is_nil(value1) and not is_nil(value2)) do
      changeset
    else
      add_error(changeset, key1, "this combination of #{key1} and #{key2} is not allowed")
      |> add_error(key2, "this combination of #{key1} and #{key2} is not allowed")
    end
  end

  def cast_date(changeset, params, field, format \\ "{YYYY}/{0M}/{0D}", opts \\ [])

  def cast_date(changeset, params, field, format, opts) when is_atom(field) do
    parse(changeset, params, Atom.to_string(field), format, opts)
  end

  def cast_date(changeset, params, field, format, opts) do
    parse(changeset, params, field, format, opts)
  end

  def cast_datetime(
        changeset,
        params,
        field,
        format \\ "{YYYY}-{0M}-{0D}T{h24}:{m}:{s}Z",
        opts \\ []
      )

  def cast_datetime(changeset, params, field, format, opts) when is_atom(field) do
    parse(changeset, params, Atom.to_string(field), format, opts)
  end

  def cast_datetime(changeset, params, field, format, opts) do
    parse(changeset, params, field, format, opts)
  end

  defp parse(changeset, params, field, format, opts \\ []) do
    value = Map.get(params, field)

    if value in [nil, ""] do
      changeset
    else
      case Timex.parse(value, format) do
        {:ok, parsed_value} ->
          Ecto.Changeset.put_change(changeset, :"#{field}", parsed_value)

        {:error, message} ->
          Logger.error("#{inspect(message)}")
          Ecto.Changeset.add_error(changeset, :"#{field}", "Invalid format")
      end
    end
  end

  def clean_upload(changeset, field, uploader) do
    change = get_change(changeset, field)
    value = Map.get(changeset.data, field)

    cond do
      changeset.valid? and not is_nil(change) and not is_nil(value) ->
        apply(uploader, :delete, [value[:file_name]])

      not changeset.valid? and is_map(change) ->
        apply(uploader, :delete, [change[:file_name]])

      true ->
        nil
    end

    changeset
  end
end
