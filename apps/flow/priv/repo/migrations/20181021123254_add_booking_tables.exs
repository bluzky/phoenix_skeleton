defmodule Flow.Repo.Migrations.AddBookingTables do
  use Ecto.Migration

  def change do
    create table(:bk_resources) do
      add(:name, :text)
      add(:url, :text)
      add(:logo, :text)
      add(:description, :text)
      add(:enabled, :boolean, default: true)
      timestamps()
    end

    create table(:bk_categories) do
      add(:name, :text)
      add(:enabled, :boolean, default: true)

      timestamps()
    end

    create table(:bk_ad_units) do
      add(:name, :text)
      add(:identifier, :text)
      add(:resource_id, references(:bk_resources, on_delete: :delete_all))
      add(:category_id, references(:bk_categories, on_delete: :nilify_all))
      add(:description, :text)
      add(:enabled, :boolean, default: true)
      add(:share_options, {:array, :text}, default: [])

      timestamps()
    end

    create table(:bk_orders) do
      add(:contract_code, :text)
      add(:name, :text)
      add(:description, :text)
      add(:department_id, references(:departments, on_delete: :nilify_all))
      add(:customer, :text)
      add(:start_date, :utc_datetime)
      add(:end_date, :utc_datetime)
      add(:status, :text)
      add(:created_by_id, references(:users, on_delete: :nilify_all))
      timestamps()
    end

    create table(:bk_booking_items) do
      add(:name, :text)
      add(:order_id, references(:bk_orders))
      add(:resource_id, references(:bk_resources, on_delete: :nilify_all))
      add(:ad_unit_id, references(:bk_ad_units, on_delete: :nilify_all))
      add(:share_option, :text)
      add(:start_at, :utc_datetime)
      add(:end_at, :utc_datetime)
      add(:description, :text)
      add(:note, :text)
      add(:status, :text)
      add(:created_by_id, references(:users, on_delete: :nilify_all))

      timestamps()
    end

    create(index(:bk_booking_items, [:order_id]))
    create(index(:bk_booking_items, [:status]))
  end
end
