defmodule Flow.Repo.Migrations.InitDatabase do
  use Ecto.Migration

  def change do
    create table(:companies) do
      add(:name, :text)
      add(:identifier, :text, null: false)
      add(:logo, :text)
      add(:email, :text)
      add(:phone, :text)
      add(:website, :text)
      add(:status, :text)

      timestamps
    end

    create(index(:companies, [:identifier], unique: true))

    # department table
    create table(:departments) do
      add(:name, :text)
      add(:company_id, references(:companies, on_delete: :delete_all))
      add(:status, :text)

      timestamps()
    end

    # end user
    create table(:users) do
      add(:first_name, :text)
      add(:last_name, :text)
      add(:full_name, :text)
      add(:username, :text, null: false)
      add(:password, :text)
      add(:email, :text)
      add(:phone, :text)
      add(:avatar, :text)
      add(:title, :text)
      add(:roles, {:array, :text})
      add(:company_id, references(:companies, on_delete: :delete_all))
      add(:department_id, references(:departments, on_delete: :delete_all))

      add(:status, :text)
      add(:reset_password_token, :text)

      timestamps()
    end

    create(index(:users, [:username], unique: true))
    create(index(:users, [:email], unique: true))

    create table(:attachments) do
      add(:filename, :text)
      add(:mime, :text)
      add(:size, :bigint)
      add(:thumb, :text)
      add(:path, :text)
      add(:storage, :text)
      add(:uploaded_by_id, references(:users, on_delete: :nilify_all))
      add(:schema, :text)
      add(:schema_id, :integer)

      timestamps()
    end

    create(index(:attachments, [:schema, :schema_id]))
  end
end
