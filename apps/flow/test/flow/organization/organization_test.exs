defmodule Flow.OrganizationTest do
  use Flow.DataCase

  alias Flow.Organization

  describe "companies" do
    alias Flow.Organization.Company

    @valid_attrs %{
      contact: "some contact",
      email: "some email",
      identifier: "some identifier",
      logo: "some logo",
      name: "some name",
      phone: "some phone",
      status: "some status",
      website: "some website"
    }
    @update_attrs %{
      contact: "some updated contact",
      email: "some updated email",
      identifier: "some updated identifier",
      logo: "some updated logo",
      name: "some updated name",
      phone: "some updated phone",
      status: "some updated status",
      website: "some updated website"
    }
    @invalid_attrs %{
      contact: nil,
      email: nil,
      identifier: nil,
      logo: nil,
      name: nil,
      phone: nil,
      status: nil,
      website: nil
    }

    def company_fixture(attrs \\ %{}) do
      {:ok, company} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organization.create_company()

      company
    end

    test "list_companies/0 returns all companies" do
      company = company_fixture()
      assert Organization.list_companies() == [company]
    end

    test "get_company!/1 returns the company with given id" do
      company = company_fixture()
      assert Organization.get_company!(company.id) == company
    end

    test "create_company/1 with valid data creates a company" do
      assert {:ok, %Company{} = company} = Organization.create_company(@valid_attrs)
      assert company.contact == "some contact"
      assert company.email == "some email"
      assert company.identifier == "some identifier"
      assert company.logo == "some logo"
      assert company.name == "some name"
      assert company.phone == "some phone"
      assert company.status == "some status"
      assert company.website == "some website"
    end

    test "create_company/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_company(@invalid_attrs)
    end

    test "update_company/2 with valid data updates the company" do
      company = company_fixture()
      assert {:ok, company} = Organization.update_company(company, @update_attrs)
      assert %Company{} = company
      assert company.contact == "some updated contact"
      assert company.email == "some updated email"
      assert company.identifier == "some updated identifier"
      assert company.logo == "some updated logo"
      assert company.name == "some updated name"
      assert company.phone == "some updated phone"
      assert company.status == "some updated status"
      assert company.website == "some updated website"
    end

    test "update_company/2 with invalid data returns error changeset" do
      company = company_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.update_company(company, @invalid_attrs)
      assert company == Organization.get_company!(company.id)
    end

    test "delete_company/1 deletes the company" do
      company = company_fixture()
      assert {:ok, %Company{}} = Organization.delete_company(company)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_company!(company.id) end
    end

    test "change_company/1 returns a company changeset" do
      company = company_fixture()
      assert %Ecto.Changeset{} = Organization.change_company(company)
    end
  end

  describe "departments" do
    alias Flow.Organization.Department

    @valid_attrs %{
      function: "some function",
      name: "some name",
      status: "some status",
      sub_function: "some sub_function"
    }
    @update_attrs %{
      function: "some updated function",
      name: "some updated name",
      status: "some updated status",
      sub_function: "some updated sub_function"
    }
    @invalid_attrs %{function: nil, name: nil, status: nil, sub_function: nil}

    def department_fixture(attrs \\ %{}) do
      {:ok, department} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organization.create_department()

      department
    end

    test "list_departments/0 returns all departments" do
      department = department_fixture()
      assert Organization.list_departments() == [department]
    end

    test "get_department!/1 returns the department with given id" do
      department = department_fixture()
      assert Organization.get_department!(department.id) == department
    end

    test "create_department/1 with valid data creates a department" do
      assert {:ok, %Department{} = department} = Organization.create_department(@valid_attrs)
      assert department.function == "some function"
      assert department.name == "some name"
      assert department.status == "some status"
      assert department.sub_function == "some sub_function"
    end

    test "create_department/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_department(@invalid_attrs)
    end

    test "update_department/2 with valid data updates the department" do
      department = department_fixture()
      assert {:ok, department} = Organization.update_department(department, @update_attrs)
      assert %Department{} = department
      assert department.function == "some updated function"
      assert department.name == "some updated name"
      assert department.status == "some updated status"
      assert department.sub_function == "some updated sub_function"
    end

    test "update_department/2 with invalid data returns error changeset" do
      department = department_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Organization.update_department(department, @invalid_attrs)

      assert department == Organization.get_department!(department.id)
    end

    test "delete_department/1 deletes the department" do
      department = department_fixture()
      assert {:ok, %Department{}} = Organization.delete_department(department)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_department!(department.id) end
    end

    test "change_department/1 returns a department changeset" do
      department = department_fixture()
      assert %Ecto.Changeset{} = Organization.change_department(department)
    end
  end
end
