defmodule Flow.AccountTest do
  use Flow.DataCase

  alias Flow.Account

  describe "users" do
    alias Flow.Account.User

    @valid_attrs %{
      address: "some address",
      avatar: "some avatar",
      dob: ~N[2010-04-17 14:00:00.000000],
      email: "some email",
      first_name: "some first_name",
      last_name: "some last_name",
      password: "some password",
      phone: "some phone",
      reset_password_token: "some reset_password_token",
      start_date: ~N[2010-04-17 14:00:00.000000],
      status: "some status",
      troles: [],
      username: "some username"
    }
    @update_attrs %{
      address: "some updated address",
      avatar: "some updated avatar",
      dob: ~N[2011-05-18 15:01:01.000000],
      email: "some updated email",
      first_name: "some updated first_name",
      last_name: "some updated last_name",
      password: "some updated password",
      phone: "some updated phone",
      reset_password_token: "some updated reset_password_token",
      start_date: ~N[2011-05-18 15:01:01.000000],
      status: "some updated status",
      troles: [],
      username: "some updated username"
    }
    @invalid_attrs %{
      address: nil,
      avatar: nil,
      dob: nil,
      email: nil,
      first_name: nil,
      last_name: nil,
      password: nil,
      phone: nil,
      reset_password_token: nil,
      start_date: nil,
      status: nil,
      troles: nil,
      username: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_attrs)
      assert user.address == "some address"
      assert user.avatar == "some avatar"
      assert user.dob == ~N[2010-04-17 14:00:00.000000]
      assert user.email == "some email"
      assert user.first_name == "some first_name"
      assert user.last_name == "some last_name"
      assert user.password == "some password"
      assert user.phone == "some phone"
      assert user.reset_password_token == "some reset_password_token"
      assert user.start_date == ~N[2010-04-17 14:00:00.000000]
      assert user.status == "some status"
      assert user.troles == []
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Account.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.address == "some updated address"
      assert user.avatar == "some updated avatar"
      assert user.dob == ~N[2011-05-18 15:01:01.000000]
      assert user.email == "some updated email"
      assert user.first_name == "some updated first_name"
      assert user.last_name == "some updated last_name"
      assert user.password == "some updated password"
      assert user.phone == "some updated phone"
      assert user.reset_password_token == "some updated reset_password_token"
      assert user.start_date == ~N[2011-05-18 15:01:01.000000]
      assert user.status == "some updated status"
      assert user.troles == []
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end
end
