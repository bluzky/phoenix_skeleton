use Mix.Config

# Configure your database
config :flow, Flow.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DBUSER"),
  password: System.get_env("DBPASSWORD"),
  database: System.get_env("DBNAME"),
  hostname: System.get_env("DBHOST"),
  port: System.get_env("DBPORT"),
  pool_size: 10
