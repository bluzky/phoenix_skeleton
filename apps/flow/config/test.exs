use Mix.Config

# Configure your database
config :flow, Flow.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "flow_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
