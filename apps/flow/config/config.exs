use Mix.Config

config :flow, ecto_repos: [Flow.Repo]

import_config "#{Mix.env()}.exs"
