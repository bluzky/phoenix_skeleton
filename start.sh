#!/usr/bin/env bash
c_dir=$PWD
. ./.env
#mix clean --all && mix deps.get && mix compile

if [ $1 ]
then
  cd apps/$1
fi

if [ $2 ]
then
  if [ "$2" = "debug" ];
  then
    iex -S mix phx.server
  else
    if [ "$2" = "release" ];
    then
      yarn install
      yarn deploy
      # cd "$c_dir/apps/$1"
      echo "Clean assets..."
      mix phx.digest.clean
      echo "Build assets..."
      mix phx.digest
      echo "Build assets done"
      mix release
      eval "$c_dir/_build/$MIX_ENV/rel/$1/bin/$1 stop"
      eval "$c_dir/_build/$MIX_ENV/rel/$1/bin/$1 start"
      echo "Ahihi"
      cd "$c_dir"
    else
      shift
      mix $@
    fi
  fi
else
  mix phx.server
fi
